<?php 
require_once '../vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

$connection = new AMQPStreamConnection('rabbitmq.selfmade.ninja', 5672, 'Adminer', 'f9iiig2zzo', 'kavinprasad_helloworld');

$channel = $connection->channel();

$channel->queue_declare('hello', false, false, false, false);

while(true){
    // $message = random_int(100000, 9999999);
    $message = readline("Enter the message : ");
    $msg = new AMQPMessage(
        $message,
        array('delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT)
    );
    
    $channel->basic_publish($msg, '', 'hello');
    
    echo "[+] Sent $message\n";

}

$channel->close();
$connection->close();
