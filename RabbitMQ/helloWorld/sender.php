<?php 
require_once '../vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

$connection = new AMQPStreamConnection('rabbitmq.selfmade.ninja', 5672, 'Adminer', 'f9iiig2zzo', 'kavinprasad_helloworld');

$channel = $connection->channel();

$channel->queue_declare('hello', false, false, false, false);

while(true){
    // $message = random_int(100000, 9999999);
    $message = readline("Enter the message : ");
    $msg = new AMQPMessage($message);
    
    $channel->basic_publish($msg, '', 'hello');
    
    echo "[+] Sent $message\n";

}

$channel->close();
$connection->close();

// import pika
// import string
// import random

// try:
//     connection = pika.BlockingConnection(pika.ConnectionParameters(host='rabbitmq.selfmade.ninja', port=5672, credentials=pika.PlainCredentials('Adminer', 'f9iiig2zzo'), virtual_host='kavinprasad_helloworld'))

//     channel = connection.channel()

//     channel.queue_declare(queue='hello')
    
//     while True:
//         try:
//             message = ''.join(random.choices(string.ascii_uppercase + string.digits, k=10))
//             # message = input("Enter the Message : ")
//             channel.basic_publish(exchange='', routing_key='hello', body=message)
//             print(" [+] Sent " + message)
//         except KeyboardInterrupt:
//             print("\nExiting...")
//             break
    
//     connection.close()

// except Exception as e:
//     print("An error occurred:", str(e)) 