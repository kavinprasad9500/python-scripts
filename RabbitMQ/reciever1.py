import pika

connection = pika.BlockingConnection(pika.ConnectionParameters(host='rabbitmq.selfmade.ninja', port=5672, credentials=pika.PlainCredentials('Adminer', 'f9iiig2zzo'), virtual_host='kavinprasad_helloworld'))

channel = connection.channel()

channel.queue_declare(queue='hello')

print(' [*] Waiting for messages. To exit press CTRL+C')

def callback(ch, method, properties, body):
    print(f" [-] Received {body}")

try:
    
    channel.basic_consume(queue='hello',
                        auto_ack=True,
                        on_message_callback=callback)

    channel.start_consuming()
except KeyboardInterrupt:
    print("\nExiting....")