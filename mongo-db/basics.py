import pymongo


# Connect to MongoDB
client = pymongo.MongoClient("mongodb://kavin:f9iiig2zzo@mongodb.selfmade.ninja:27017/?authSource=users")
db = client["kavin_employees"] # Create a database


# print(client.list_database_names()) #List DATABASE NAME in array
dblist = client.list_database_names()
if "kavin_employees" in dblist:
  print("The database exists.") #Check Once If already DataBase in collectiohn 
  
  
col = db["employees"] # Create a collection and insert a document
# print(db.list_collection_names()) #List the collectoins
if "employees" in db.list_collection_names(): # Check if the collection exists
    print("The collection exists.")


########### INSERT A DATA  ###########
# Insert a document into the collection
employee_data = {
    "name": "John Doe",
    "email": "john@example.com",
    "position": "Developer"
}
# x = col.insert_one(employee_data) # Insert the document into the collection
# print(x.inserted_id)

mydict = { "name": "John", "address": "Highway 37" }
# y = col.insert_one(mydict)
# print(y.inserted_id)

mylist = [
  { "_id": 1, "name": "John", "address": "Highway 37"},
  { "_id": 2, "name": "Peter", "address": "Lowstreet 27"},
  { "_id": 3, "name": "Amy", "address": "Apple st 652"},
  { "_id": 4, "name": "Hannah", "address": "Mountain 21"},
  { "_id": 5, "name": "Michael", "address": "Valley 345"},
  { "_id": 6, "name": "Sandy", "address": "Ocean blvd 2"},
  { "_id": 7, "name": "Betty", "address": "Green Grass 1"},
  { "_id": 8, "name": "Richard", "address": "Sky st 331"},
  { "_id": 9, "name": "Susan", "address": "One way 98"},
  { "_id": 10, "name": "Vicky", "address": "Yellow Garden 2"},
  { "_id": 11, "name": "Ben", "address": "Park Lane 38"},
  { "_id": 12, "name": "William", "address": "Central st 954"},
  { "_id": 13, "name": "Chuck", "address": "Main Road 989"},
  { "_id": 14, "name": "Viola", "address": "Sideway 1633"}
]
# #print list of the _id values of the inserted documents:
# z = col.insert_many(mylist)
# print(z.inserted_ids)


#########   Seach Data    ######## 
# print(col.find_one()) #Find One Data
# for i in col.find(): #Find All Data
#   print(i)
# for i in col.find({},{ "_id": 0, "name": 1, "address": 1 }): #SHOW ONLY NAME AND ADDRESS
#   print(i)
# for i in col.find({},{"address": 0}): #SHOW OTHER DATA EXCEPT ADDRESS
#     print(i)
# for i in col.find({},{ "name": 1, "address": 0 }):
#   print(i)

#### WITH USING QUERY ####
# query1 = { "name": "Vicky" }
# mydoc = col.find(query)
# for x in mydoc:
#   print(x)

# query2= {"address": {"$gt": "S"}}
# query2= {"_id": {"$gte": 5}}
# query2= {"_id": {"$lt": 5}}
# doc = col.find(query2)
# for x in doc:
#     print(x)

query3 = {"name": {"$regex": "^S"}}
doc1=col.find(query3)
for x in doc1:
    print(x)

###### SORTING ####
# doc3 = col.find().sort("name") ## Sort by name
# for x in doc3:
#     print(x)

# doc4 = col.find().sort("name", -1) ## Sort by name ascending
# for x in doc4:
#     print(x)
    
# doc5 = col.find().sort("name", 1) ## Sort by name descending
# for x in doc5:
#     print(x)



#### DELETION ####
# query4={"name": "Sandy"}
# col.delete_one(query4)

# query5={"name": {"$regex": "^S"}}
# d = col.delete_many(query5)
# print(d.deleted_count, " documents deleted.")


# DELETE COLLECTION 
# col.drop()



####### UPDATE #######
# query6 = { "address": "Valley 345" }
# newvalue = { "$set": { "address": "Canyon 123" } }
# col.update_one(query6, newvalue)
# for i in col.find():
#   print(i)


# query7= {"address":{"$regex": "^S"}}
# newvalues = {"$set":{"name": "Kavin"}}
# updateMany= col.update_many(query7, newvalues)
# print(updateMany.modified_count, "document updated")


#### LIMITS ####
# result = col.find().limit(5)

# #print the result:
# for x in result:
#   print(x)


client.close() # Close the MongoDB connection