from pymemcache.client import base
import requests
import time

start_time = time.time()
client = base.Client(('memcached.selfmade.ninja', 11211))
result = client.get('response')


if result is None:
    response = requests.get("https://test.selfmade.chat/api/employees")
    client.set('response', 'some value')
# https://test.selfmade.chat/api/employees
# client.get('response')
end_time = time.time()
execution_time_microseconds = (end_time - start_time)
print(client.get('response'))
# print(response.json()[0]["firstName"])
print(f"Execution Time: {execution_time_microseconds:.8f} microseconds")



# └❯ python test.py 
# b'some value'
# Execution Time: 0.0557 microseconds

# └❯ python test.py 
# b'some value'
# Execution Time: 0.0006 microseconds